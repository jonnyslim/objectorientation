package Matrix;

import java.io.*;
import java.util.*;
class EntrySort {

    public void mergesort(ArrayList<SparseMatrix.Entry> arr, int i, int j) {
        int mid = 0;
        if (i < j) {
            mid  = (i + j) / 2;
            mergesort(arr, i, mid);
            mergesort(arr, mid+1, j);
            merge(arr, i, mid, j);
        }
    }

    public void merge(ArrayList<SparseMatrix.Entry> arr, int i, int mid, int j) {
        int l = i;
        int r = j;
        int m = mid + 1;
        int k = l;
        ArrayList<SparseMatrix.Entry> temp = new ArrayList<SparseMatrix.Entry>();
        for (int ix = 0 ; ix < arr.size() ; ix++) {temp.add(null);}

        while (l <= mid && m <= r) {
            if (arr.get(l).getPosition() <= arr.get(m).getPosition()) {
                temp.set(k, arr.get(l));
                k++; l++;
            }
            else {
                temp.set(k, arr.get(m));
                k++; m++;
            }
        }

        while (l <= mid) {
            temp.set(k, arr.get(l));
            k++; l++;
        }

        while (m <= r) {
            temp.set(k, arr.get(m));
            k++; m++;
        }

        for (int i1 = i; i1 <= j; i1++) {
            arr.set(i1, temp.get(i1));
        }
    }

}
