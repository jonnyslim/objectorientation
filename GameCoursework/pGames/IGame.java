package pGames;

import pRandoms.*;

public interface IGame {

	public void initialiseGame() throws Exception;

	public void mainGame() throws Exception;

	public void declareWinner() throws Exception;

}