package pGames;

import pRandoms.*;
import java.io.*;
import java.util.*;

public class GameFactory {

	public static IGame createGame(String type) throws Exception {
		IGame game = null;
		switch(type) {
			case "d":
				game = new DIE();
				break;
			case "c":
				game = new CARD();
				break;
			default:
				throw new Exception("Input is not recognised");
		}
		return game;
	}
}