package pGames;

import pRandoms.*;
import java.io.*;
import java.util.*;

public class GameMain {
	
	public static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

 	public static RandomInterface r = new LinearCongruentialGenerator();

 	public static void main(String[] args) throws Exception {

 		System.out.print("Card (c) or Die (d) game? ");
 		String input = br.readLine();

 		IGame game = GameFactory.createGame(input);

 		game.initialiseGame();

 		game.mainGame();

 		game.declareWinner();

 	}
}