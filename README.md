Project contains 2 separate pieces of work:
    1. Game Coursework 
    2. Matrix Coursework

The Game project is intended to apply traditional OO design patterns and principles

The Matrix Coursework is an investigation of different data structures representing a "Sparse Matrix", where only non-zero elements of a matrix are stored. Different operations are then performed on the sparse matrix to evaluate the rubustness and consistency of its structure
